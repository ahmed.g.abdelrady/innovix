﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using Innovix_task.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Innovix_task.Data
{
    public class InitializeApplicationDb
    {
      public static void SeedEverythingAsync(ApplicationDbContext context)
        {
            try
            {
               
                context.Database.EnsureCreated();
               SeedRoomTypes(context);
               SeedRooms(context);
               SeedRoomRates(context);
               SeedMealPlans(context);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private  static async void SeedMealPlans(ApplicationDbContext context)
        {
            if (context.MealPlans.Any())
            {
                return; // Db has been seeded
            }

            var mealPlans = new List<MealPlan>
            {
                new MealPlan
                {
                    Name = "Half Board", AdultExtraCharge = 0, ChildExtraCharge = 0,
                    IsActive = true, IsDeleted = false
                },
                new MealPlan
                {
                    Name = "Full Board", AdultExtraCharge = 10, ChildExtraCharge = 5,
                    IsActive = true, IsDeleted = false
                },
                new MealPlan
                {
                    Name = "All Inclusive", AdultExtraCharge = 15, ChildExtraCharge = 8,
                    IsActive = true, IsDeleted = false
                },
            };
           context.MealPlans.AddRange(mealPlans);
           context.SaveChanges();

        }

      private static void SeedRoomRates(ApplicationDbContext context)
        {
            if (context.RoomRates.Any())
            {
                return; // Db has been seeded
            }

            var Rates = new List<RoomRate>();
            List<DateTime[]> Dates = new List<DateTime[]>
            {
                new DateTime[] {new DateTime(2020, 10, 1), new DateTime(2020, 12, 20)},
                new DateTime[] {new DateTime(2020, 12, 21), new DateTime(2021, 1, 5)},
                new DateTime[] {new DateTime(2021, 1, 6), new DateTime(2021, 4, 30)},
                new DateTime[] {new DateTime(2021, 5, 1), new DateTime(2021, 9, 30)},
            };

            List<int[]> prices = new List<int[]>
            {
                new int[] {50, 60, 80, 90},
                new int[] {60, 70, 95, 120 },
                new int[] {40, 50, 65, 80 },
                new int[] {50, 60, 90, 100 }
            };
            var rooms = context.Rooms.ToList();
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    var Rate =new RoomRate
                    {
                        From = Dates[i][0],
                        To = Dates[i][1],
                        Rate = prices[i][j],
                        RoomId =rooms[j].Id
                    };
                    context.RoomRates.Add(Rate);
                }

            }
            context.SaveChanges();
        }

      private static void SeedRooms(ApplicationDbContext context)
        {
            if (context.Rooms.Any())
            {
                return; // Db has been seeded
            }

            var rooms = new List<Room>
            {
                new Room { 
                    RoomTypeId = 1 ,
                    DefaultRate = 50,
                    IsActive = true,
                    MaxAdultsNumber = 2,
                    MaxChildsNumber = 1,
                    IsDeleted = false
                },
                new Room { 
                    RoomTypeId = 2 ,
                    DefaultRate = 60,
                    IsActive = true,
                    MaxAdultsNumber = 2,
                    MaxChildsNumber = 1,
                    IsDeleted = false
                },
                new Room { 
                    RoomTypeId = 3 ,
                    DefaultRate = 80,
                    IsActive = true,
                    MaxAdultsNumber = 2,
                    MaxChildsNumber = 1,
                    IsDeleted = false
                },
                new Room { 
                    RoomTypeId = 4 ,
                    DefaultRate = 90,
                    IsActive = true,
                    MaxAdultsNumber = 2,
                    MaxChildsNumber = 1,
                    IsDeleted = false
                }
            };
            context.Rooms.AddRange(rooms);
            context.SaveChanges();
        }

      private static void SeedRoomTypes(ApplicationDbContext context)
        {
            if (context.RoomTypes.Any())
            {
                return; // Db has been seeded
            }

            var types = new List<RoomType>
            {
                new RoomType {Name = "Standard Room"},
                new RoomType {Name = "Pool View"},
                new RoomType {Name = "Sea View"},
                new RoomType {Name = "Royal Suite"}
            };
            context.RoomTypes.AddRange(types);
                           context.SaveChanges();
        }
    }
}
