﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Innovix_task.Data.Entities
{
    public class MealPlan
    {
        public MealPlan()
        {
            Reservations = new HashSet<Reservation>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        //public string Discription { get; set; }
        //public string Image { get; set; }
        //public decimal Price { get; set; }
        //public int MealTypeId { get; set; }
        public decimal  AdultExtraCharge { get; set; }
        public decimal  ChildExtraCharge { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        //public virtual MealType MealType { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
