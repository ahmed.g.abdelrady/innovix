﻿using System;
using System.Reflection.Metadata.Ecma335;

namespace Innovix_task.Data.Entities
{
    public class RoomRate
    {
        public int Id { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal Rate { get; set; }
        public int RoomId { get; set; }
        public virtual Room Room { get; set; }
    }
}