﻿using System;

namespace Innovix_task.Data.Entities
{
    public class Reservation
    {
        public int Id { get; set; }
        public int GuestId { get; set; }
        public int RoomId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public int AdultsNumber { get; set; }
        public int ChildsNumber { get; set; }
        public bool IsFinished { get; set; }
        public bool IsPaid { get; set; }
        public int MealPlanId { get; set; }
        public virtual Guest Guest { get; set; }
        public virtual Room Room { get; set; }
        public virtual MealPlan MealPlan { get; set; }
    }
}