﻿using System.Collections.Generic;

namespace Innovix_task.Data.Entities
{
    public class RoomType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
    }
}