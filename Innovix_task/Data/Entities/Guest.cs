﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Innovix_task.Data.Entities
{
    public class Guest
    {
        public Guest()
        {
            Reservations = new HashSet<Reservation>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string UserId { get; set; }
        public IdentityUser User { get; set; }
      
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
