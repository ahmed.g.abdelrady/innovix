﻿using System.Collections.Generic;

namespace Innovix_task.Data.Entities
{
    public class Room
    {
        public Room()
        {
            RoomRates = new List<RoomRate>();
            Reservations = new List<Reservation>();
        }
        public int Id { get; set; }
        public int MaxAdultsNumber { get; set; }
        public int MaxChildsNumber { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public int  RoomTypeId { get; set; }
        public decimal  DefaultRate { get; set; }
        public virtual RoomType RoomType { get; set; }
        public virtual ICollection<RoomRate> RoomRates { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}