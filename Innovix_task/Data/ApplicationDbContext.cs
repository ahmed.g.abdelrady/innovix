﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Innovix_task.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Innovix_task.Data
{
    public class ApplicationDbContext : IdentityDbContext//<AppUser,AppRole,int,AppUserClaim, AppUserRole, AppUserLogin, AppRoleClaim, AppUserToken>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Guest> Guests { get; set; }
        public virtual DbSet<MealPlan> MealPlans { get; set; }
        //public virtual DbSet<MealType> MealTypes { get; set; }
        public virtual DbSet<RoomRate> RoomRates { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomType> RoomTypes { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

                base.OnModelCreating(modelBuilder);
            
            #region Configurations

            modelBuilder.Entity<Reservation>()
                .HasOne(x => x.MealPlan)
                .WithMany(x => x.Reservations)
                .HasForeignKey(x => x.MealPlanId);

            modelBuilder.Entity<Reservation>()
                .HasOne(x => x.Guest)
                .WithMany(x => x.Reservations)
                .HasForeignKey(x => x.GuestId);

            modelBuilder.Entity<Reservation>()
                .HasOne(x => x.Room)
                .WithMany(x => x.Reservations)
                .HasForeignKey(x => x.RoomId);
            
            modelBuilder.Entity<Room>()
                .HasOne(x => x.RoomType)
                .WithMany(x => x.Rooms)
                .HasForeignKey(x => x.RoomTypeId);

            modelBuilder.Entity<RoomRate>()
                .HasOne(x => x.Room)
                .WithMany(x => x.RoomRates)
                .HasForeignKey(x => x.RoomId);

            //modelBuilder.Entity<MealPlan>()
            //    .HasOne(a => a.MealType)
            //    .WithMany(b => b.Meals)
            //    .HasForeignKey(b => b.MealTypeId);
            #endregion
        }
    }
}
