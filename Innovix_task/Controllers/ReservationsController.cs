﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Innovix_task.Data;
using Innovix_task.Data.Entities;
using Innovix_task.Models;
using Microsoft.AspNetCore.Http;

namespace Innovix_task.Controllers
{
    public class ReservationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReservationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Reservations
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Reservations.Include(r => r.Guest).Include(r => r.Room);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Reservations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservation = await _context.Reservations
                .Include(r => r.Guest)
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return NotFound();
            }

            return View(reservation);
        }

        // GET: Reservations/Create
        public IActionResult Create()
        {
            ViewData["MealsPlans"] = new SelectList(_context.MealPlans, "Id", "Name");
            var rooms = _context.Rooms.Include(x => x.RoomType).Select(x => new {Id = x.Id, Name = x.RoomType.Name});
            ViewData["Rooms"] = new SelectList(rooms, "Id", "Name");
            var reservationVM = new List<ReservationVM>
            {
                new ReservationVM {FromDate = DateTime.Now, ToDate = DateTime.Now},
                new ReservationVM {FromDate = DateTime.Now, ToDate = DateTime.Now},
                new ReservationVM {FromDate = DateTime.Now, ToDate = DateTime.Now}
            };

            return View(reservationVM);
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm]List<ReservationVM> reservationVm)
        {
            if (ModelState.IsValid)
            {
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(reservationVm);
        }

        // GET: Reservations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservation = await _context.Reservations.FindAsync(id);
            if (reservation == null)
            {
                return NotFound();
            }
            ViewData["GuestId"] = new SelectList(_context.Guests, "Id", "Id", reservation.GuestId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Id", reservation.RoomId);
            return View(reservation);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,GuestId,RoomId,From,To,AdultsNumber,ChildsNumber,IsFinished,IsPaid")] Reservation reservation)
        {
            if (id != reservation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reservation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReservationExists(reservation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GuestId"] = new SelectList(_context.Guests, "Id", "Id", reservation.GuestId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Id", reservation.RoomId);
            return View(reservation);
        }

        // GET: Reservations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservation = await _context.Reservations
                .Include(r => r.Guest)
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservation == null)
            {
                return NotFound();
            }

            return View(reservation);
        }
        // POST: Reservations/Delete/5
        [HttpPost]
        public async Task<IActionResult> CalculatePrice([FromBody] List<ReservationVM> reservationVms)
        {
            decimal total = 0;
            foreach (var detail in reservationVms)
            {
                if (detail.AdultsNumber == 0 && detail.ChildsNumber == 0)
                {
                    continue;
                }
                // Get Reserved Room
                var Room = await _context.Rooms
                    .Where(x => x.Id == detail.RoomId)
                    .FirstOrDefaultAsync();

                if (Room != null)
                {
                    if (detail.AdultsNumber > Room.MaxAdultsNumber || detail.ChildsNumber > Room.MaxChildsNumber)
                    {
                        return Json(new Response<int> { Success = false ,Message = "Max Number Of Adult or Childs Exceeded"});
                    }
                    // Calculate Room rate if no rate found in selected date 
                    // select default Rate for this Room
                    var RoomRate = Room.RoomRates
                        .Where(y => y.From >= detail.FromDate && y.To <= detail.ToDate)
                        .OrderByDescending(x=>x.Rate)
                        .FirstOrDefault();
                    total += RoomRate == null ?
                        Room.DefaultRate : RoomRate.Rate;
                }
                // calculate meal plan fore adult and childs
                var mealPlan = await _context.MealPlans.FindAsync(detail.MealPlanId);
                if (mealPlan != null)
                {
                    total += detail.AdultsNumber * mealPlan.AdultExtraCharge;
                    total += detail.ChildsNumber * mealPlan.ChildExtraCharge;
                }

            }
            return Json(new Response<decimal> { Success = true, Data = total ,Message = ""});
        }

       // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reservation = await _context.Reservations.FindAsync(id);
            _context.Reservations.Remove(reservation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReservationExists(int id)
        {
            return _context.Reservations.Any(e => e.Id == id);
        }
    }
}
