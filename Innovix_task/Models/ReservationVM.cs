﻿using System;
using System.Collections.Generic;

namespace Innovix_task.Models
{
    public class ReservationVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        
        public int RoomId { get; set; }
        public int MealPlanId { get; set; }
        public int AdultsNumber { get; set; }
        public int ChildsNumber { get; set; }
    }
}