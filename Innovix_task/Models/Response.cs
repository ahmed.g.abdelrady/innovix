﻿namespace Innovix_task.Models
{
    public class Response<T>
    {
        public string Message { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}